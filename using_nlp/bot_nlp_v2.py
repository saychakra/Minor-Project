from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import pandas as pd
import numpy as np

ps = PorterStemmer()

# Entering the symptom in string format
inp_sent = input("Enter your symptom: ")
inp_sent = inp_sent.lower()

# tokenizing the words
words = word_tokenize(inp_sent)

# removing stop words
stop_words = set(stopwords.words("english"))
stop_list = []
for w in stop_words:
    stop_list.append(w)
# adding some extra words for the job
extra_words = ["i", "feel", "like", "am", "i'm", "having", "think", "so", "probably", "very", "much"]
stop_list.extend(extra_words)
filtered_sentence = [w for w in words if w not in stop_list]
# print("\nAfter removing stopwords: ", filtered_sentence)

# stemming the input text
symp_inp_list = []
for w in filtered_sentence:
    stemmed_word = ps.stem(w)
    symp_inp_list.append(stemmed_word)
print("\nAfter stemming: ", symp_inp_list)

# converting the list into a string for reducing complexity during matching with the dataframe column
symptom = ' '.join(symp_inp_list)
print ("\nFinal Symptom: ", symptom)
print("\n\n\n")

# loading dataset
symptoms_df = pd.read_csv('../medical_dataset/Final_Dataset/symptoms.csv')

# extracting the symptom column from the dataset
symp_cols = []
for w in symptoms_df['symptom']:
	symp_cols.append(w)

# removing stop words from the symp_cols
filtered_symps = [w for w in symp_cols if w not in stop_list]

# stemming the filtered_symps
filtered_symps_list = []
for w in filtered_symps:
	stem = ps.stem(w)
	filtered_symps_list.append(stem)

print(filtered_symps_list)