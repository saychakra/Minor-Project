from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import pandas as pd
import numpy as np

ps = PorterStemmer()

# Entering the symptom in string format
inp_sent = input("Enter your symptom: ")
inp_sent = inp_sent.lower()

# tokenizing the words
words = word_tokenize(inp_sent)

# removing stop words
stop_words = set(stopwords.words("english"))
stop_list = []
for w in stop_words:
    stop_list.append(w)
# adding some extra words for the job
extra_words = ["i", "feel", "like", "am", "i'm", "having", "think", "so", "probably", "very", "much", "most", "within", "in", "on", "inside"]
stop_list.extend(extra_words)

filtered_sentence = [w for w in words if w not in stop_list]
print(filtered_sentence)

# stemming the words
# generating the symptoms input
symp_inp_list = []
for w in filtered_sentence:
    stemmed_word = ps.stem(w)
    symp_inp_list.append(stemmed_word)

# converting the list into a string for reducing complexity durin matchin with the dataframe column
symptom = ' '.join(symp_inp_list)
print ("\nFinal Symptom: ", symptom)

# loading symptoms dataset
symptoms_df = pd.read_csv('../medical_dataset/Final_Dataset/symptoms.csv')
symp_2 = symptoms_df

# removing stop words from the symp_cols
symp_2['symptom'] = symp_2['symptom'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop_list)]))
# print(symp_2)

# stemming the symptoms column
# symp_2['tokenized'] = symp_2['symptom'].apply(lambda x: filter(None, x.split(" ")))
# symp_2['stemmed_symp'] = symp_2['tokenized'].apply(lambda x: [ps.stem(y) for y in x])
# symp_2['Symp_sent'] = symp_2['stemmed_symp'].apply(lambda x: " ".join(x))

# overwriting the symptom column finally with stemmed values
symp_2['symptom'] = symp_2['symptom'].apply(lambda x: filter(None, x.split(" ")))
symp_2['symptom'] = symp_2['symptom'].apply(lambda x: [ps.stem(y) for y in x])
symp_2['symptom'] = symp_2['symptom'].apply(lambda x: " ".join(x))

print(symp_2)

# searching for the symptom words within the dataset
print (symp_2[symp_2['symptom'].str.contains(symptom)])